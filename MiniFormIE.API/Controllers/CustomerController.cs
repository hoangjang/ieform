﻿using MiniFormIE.Data.Repositories;
using MiniFormIE.Domain;
using MiniIEForm.Service;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace MiniFormIE.API.Controllers
{
    [RoutePrefix("api/v1/customer")]
    public class CustomerController : ApiController
    {
        private IIncomeAndExpenditureService _incomeAndExpenditureService;
        private IAssessmentService _assessmentService;

        public CustomerController(IIncomeAndExpenditureService incomeAndExpenditureService,
                                  IAssessmentService assessmentService)
        {
            _incomeAndExpenditureService = incomeAndExpenditureService;
            _assessmentService = assessmentService;
        }

        [Route("{customerid}/incomeandexpenditureform/{id}")]
        public IHttpActionResult GetIncomeAndExpenditureForm(int customerId, int id)
        {
            List<IncomeAndExpenditure> incomeAndExpenditures = _incomeAndExpenditureService.GetIncomeAndExpenditureForm(customerId ,id);
            if (incomeAndExpenditures == null)
            {
                return NotFound();
            }
            var finalResult = new JObject();
            foreach (var ieType in incomeAndExpenditures)
            {
                finalResult.Add(ieType.Type, JToken.FromObject(ieType));
            }

            //return Ok(finalResult);
            return Ok(incomeAndExpenditures);
        }

        [Route("{customerid}")]
        public IEnumerable<IEAssessment> GetAssessments(int customerId)
        {
            return _assessmentService.GetAllAssessmentsByCustomerId(customerId);
        }

        [Route("")]
        public IHttpActionResult GetEmptyIncomeAndExpenditureForm()
        {
            var query = _incomeAndExpenditureService.GetEmptyIncomeAndExpenditureForm();
            return Ok();
        }
    }
}