﻿using MiniFormIE.Data.Repositories;
using MiniFormIE.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniIEForm.Service
{
    public class DataService:IDataService
    {
        IDataRepository _repo;
        public DataService(IDataRepository repo)
        {
            _repo = repo;
        }

        public IEnumerable<IEData> GetAllData()
        {
            return _repo.GetAll();
        }

        public IEData GetDataById(int id)
        {
            return _repo.GetById(id);
        }
    }

    public interface IDataService
    {
        IEData GetDataById(int id);
        IEnumerable<IEData> GetAllData();
    }
}
