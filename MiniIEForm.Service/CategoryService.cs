﻿using MiniFormIE.Data.Repositories;
using MiniFormIE.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniIEForm.Service
{
    public class CategoryService:ICategoryService
    {
        ICategoryRepository _repo;
        public CategoryService(ICategoryRepository repo)
        {
            _repo = repo;
        }

        public IEnumerable<IECategory> GetAllCategoies()
        {
            return _repo.GetAll();
        }

        public IECategory GetCategoryById(int id)
        {
            return _repo.GetById(id);
        }
    }

    public interface ICategoryService
    {
        IECategory GetCategoryById(int id);
        IEnumerable<IECategory> GetAllCategoies();
    }
}
