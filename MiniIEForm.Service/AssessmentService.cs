﻿using MiniFormIE.Data.Repositories;
using MiniFormIE.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniIEForm.Service
{
    public class AssessmentService : IAssessmentService
    {
        private IAssessmentRepository _repo;

        public AssessmentService(IAssessmentRepository repo)
        {
            _repo = repo;
        }
        public IEnumerable<IEAssessment> GetAllAssessmentsByCustomerId(int customerId)
        {
            return _repo.GetAll().Where(x => x.CustomerId == customerId);
        }
    }

    public interface IAssessmentService
    {
        IEnumerable<IEAssessment> GetAllAssessmentsByCustomerId(int customerId);
    }
}
