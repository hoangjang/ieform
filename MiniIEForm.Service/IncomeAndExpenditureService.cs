﻿using MiniFormIE.Data.Repositories;
using MiniFormIE.Domain;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniIEForm.Service
{
    public class IncomeAndExpenditureService : IIncomeAndExpenditureService
    {
        private IAssessmentRepository _assessmentRepo;
        private ICategoryRepository _categoryRepo;
        private IDataRepository _dataRepo;
        private ITypeRepository _typeRepo;

        public IncomeAndExpenditureService(IAssessmentRepository assessmentRepo,
                                 ICategoryRepository categoryRepo,
                                 IDataRepository dataRepo,
                                 ITypeRepository typeRepo)
        {
            _assessmentRepo = assessmentRepo;
            _categoryRepo = categoryRepo;
            _dataRepo = dataRepo;
            _typeRepo = typeRepo;
        }

        public IncomeAndExpenditure GetEmptyIncomeAndExpenditureForm()
        {
            var queryResult = (from c in _categoryRepo.GetAll()
                              join t in _typeRepo.GetAll() on c.IETypeId equals t.Id
                              orderby t.Id
                              select new AssessmentDetail
                              {
                                  CategoryName = c.Name,
                                  CategoryType = t.Type
                              }).ToList();
            List<IncomeAndExpenditure> types = new List<IncomeAndExpenditure>();
            var result = queryResult.GroupBy(t => t.CategoryType).Select(g => g.ToList()).ToList();
            
            foreach (var r in result)
            {
            }
            throw new NotImplementedException();
        }

        public List<IncomeAndExpenditure> GetIncomeAndExpenditureForm(int customerId, int id)
        {
            var queryResult = (from a in _assessmentRepo.GetAll()
                               join d in _dataRepo.GetAll() on a.Id equals d.IEAssessmentId
                               join c in _categoryRepo.GetAll() on d.IECategoryId equals c.Id
                               join t in _typeRepo.GetAll() on c.IETypeId equals t.Id
                               where a.Id == id && a.CustomerId == customerId 
                               orderby t.Id
                               select new AssessmentDetail
                               {
                                   Amount = d.Value,
                                   CategoryName = c.Name,
                                   CategoryType = t.Type,
                                   InsertTimestamp = a.Insert_Ts
                               }).ToList();

            if (queryResult.Count == 0)
            {
                return null;
            }

            List<IncomeAndExpenditure> incomeAndExpenditures = new List<IncomeAndExpenditure>();
            string currentType = queryResult.FirstOrDefault().CategoryType;
            IncomeAndExpenditure incomeAndExpenditure = new IncomeAndExpenditure()
            {
                Categories = new List<IncomeAndExpenditureCategory>(),
                Type = currentType
            };

            for (var i = 0; i < queryResult.Count; i++)
            {
                var record = queryResult[i];
                if (currentType != record.CategoryType)
                {
                    incomeAndExpenditures.Add(incomeAndExpenditure);
                    currentType = record.CategoryType;
                    incomeAndExpenditure = new IncomeAndExpenditure()
                    {
                        Categories = new List<IncomeAndExpenditureCategory>(),
                        Type = currentType
                    };
                }
                else if (i == queryResult.Count - 1)
                {
                    incomeAndExpenditures.Add(incomeAndExpenditure);
                }

                incomeAndExpenditure.Categories.Add(new IncomeAndExpenditureCategory
                {
                    Value = record.Amount,
                    Name = record.CategoryName
                });
            }

            return incomeAndExpenditures;
        }
    }

    public interface IIncomeAndExpenditureService
    {
        List<IncomeAndExpenditure> GetIncomeAndExpenditureForm(int customerId, int id);
        IncomeAndExpenditure GetEmptyIncomeAndExpenditureForm();
    }
}
