﻿using MiniFormIE.Data.Repositories;
using MiniFormIE.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniIEForm.Service
{
    public class TypeService : ITypeService
    {
        ITypeRepository _repo;
        public TypeService(ITypeRepository repo)
        {
            _repo = repo;
        }

        public IEnumerable<IEType> GetAllType()
        {
            return _repo.GetAll();
        }

        public IEType GetTypeById(int id)
        {
            return _repo.GetById(id);
        }
    }

    public interface ITypeService
    {
        IEType GetTypeById(int id);
        IEnumerable<IEType> GetAllType();
    }
}
