﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniFormIE.Domain
{
    public class IEData
    {
        public int Id { get; set; }
        public decimal Value { get; set; }
        public int IEAssessmentId { get; set; }
        public int IECategoryId { get; set; }
        public DateTime Insert_Ts { get; set; }
        public DateTime Update_Ts { get; set; }
    }
}
