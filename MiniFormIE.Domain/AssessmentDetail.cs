﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniFormIE.Domain
{
    public class AssessmentDetail
    {
        public string CategoryName { get; set; }
        public decimal Amount { get; set; }
        public string CategoryType { get; set; }
        public DateTime InsertTimestamp { get; set; }
    }
}
