﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniFormIE.Domain
{
    public class IEType
    {
        public int Id { get; set; }
        public string Type { get; set; }
        public DateTime Insert_Ts { get; set; }
        public DateTime Update_Ts { get; set; }
    }
}
