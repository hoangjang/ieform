﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniFormIE.Domain
{
    public class IncomeAndExpenditure
    {
        //[JsonIgnore]
        public string Type { get; set; }
        public List<IncomeAndExpenditureCategory> Categories { get; set; }

        public decimal Total
        {
            get { return Categories.Sum(x => x.Value); }
        }
    }
    public class IncomeAndExpenditureCategory
    {
        public string Name { get; set; }
        public decimal Value { get; set; }
    }
}
