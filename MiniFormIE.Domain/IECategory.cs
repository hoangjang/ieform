﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniFormIE.Domain
{
    public class IECategory
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int IETypeId { get; set; }
        //public int IEVersionId { get; set; }
        public int Order { get; set; }
        public DateTime Insert_Ts { get; set; }
        public DateTime Update_Ts { get; set; }
    }
}
