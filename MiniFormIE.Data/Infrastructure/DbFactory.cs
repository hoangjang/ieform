﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniFormIE.Data.Infrastructure
{
    public class DbFactory : IDbFactory
    {
        private IEFormContext dataContext;

        public IEFormContext Init()
        {
            return dataContext ?? (dataContext = new IEFormContext());
        }
    }
}
