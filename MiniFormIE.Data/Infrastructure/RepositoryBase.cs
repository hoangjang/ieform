﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniFormIE.Data.Infrastructure
{
    public class RepositoryBase<T> where T: class
    {
        private IEFormContext dbContext;
        private DbSet<T> dbSet;

        public IEFormContext DataContext { get { return dbContext ?? (dbContext = DbFactory.Init()); } }
        public IDbFactory DbFactory { get; private set; }

        public RepositoryBase(IDbFactory dbFactory)
        {
            DbFactory = dbFactory;
            dbSet = DataContext.Set<T>();
        }

        public virtual T GetById(int id)
        {
            return dbSet.Find(id);
        }

        public virtual IEnumerable<T> GetAll()
        {
            return dbSet.ToList();
        }

        public virtual void Add(T entity)
        {
            dbSet.Add(entity);
        }

        public void SaveChanges()
        {
            DataContext.Commit();
        }
    }
}
