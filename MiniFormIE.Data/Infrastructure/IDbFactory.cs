﻿namespace MiniFormIE.Data.Infrastructure
{
    public interface IDbFactory
    {
        IEFormContext Init();
    }
}