﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniFormIE.Data.Infrastructure
{
    public interface IRepository<T> where T: class
    {
        T GetById(int id);
        IEnumerable<T> GetAll();
        void Add(T entity);
        void SaveChanges();
    }
}
