﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.Entity;
using MiniFormIE.Domain;

namespace MiniFormIE.Data
{
    public class IEDBInitializer: DropCreateDatabaseIfModelChanges<IEFormContext>
    {
        protected override void Seed(IEFormContext context)
        {
            GetIETypes().ForEach(x => context.IETypes.Add(x));
            GetIECategories().ForEach(x => context.IECategories.Add(x));
            GetIEAssessments().ForEach(x => context.IEAssessments.Add(x));
            GetIEDatas().ForEach(x => context.IEDatas.Add(x));
            context.Commit();
        }

        private List<IEData> GetIEDatas()
        {
            List<IEData> result = new List<IEData>();
            result.Add(new IEData()
            {
                IEAssessmentId = 1,
                IECategoryId = 1,
                Value = 55.55m,
                Insert_Ts = DateTime.Now
            });
            result.Add(new IEData()
            {
                IEAssessmentId = 1,
                IECategoryId = 2,
                Value = 55.55m,
                Insert_Ts = DateTime.Now
            });
            result.Add(new IEData()
            {
                IEAssessmentId = 1,
                IECategoryId = 3,
                Value = 55.55m,
                Insert_Ts = DateTime.Now
            });
            result.Add(new IEData()
            {
                IEAssessmentId = 1,
                IECategoryId = 4,
                Value = 55.55m,
                Insert_Ts = DateTime.Now
            });
            result.Add(new IEData()
            {
                IEAssessmentId = 1,
                IECategoryId = 5,
                Value = 55.55m,
                Insert_Ts = DateTime.Now
            });
            result.Add(new IEData()
            {
                IEAssessmentId = 1,
                IECategoryId = 6,
                Value = 55.55m,
                Insert_Ts = DateTime.Now
            });
            result.Add(new IEData()
            {
                IEAssessmentId = 1,
                IECategoryId = 7,
                Value = 55.55m,
                Insert_Ts = DateTime.Now
            });
            result.Add(new IEData()
            {
                IEAssessmentId = 1,
                IECategoryId = 8,
                Value = 55.55m,
                Insert_Ts = DateTime.Now
            });


            result.Add(new IEData()
            {
                IEAssessmentId = 2,
                IECategoryId = 1,
                Value = 586.25m,
                Insert_Ts = DateTime.Now
            });
            result.Add(new IEData()
            {
                IEAssessmentId = 2,
                IECategoryId = 2,
                Value = 51.87m,
                Insert_Ts = DateTime.Now
            });
            result.Add(new IEData()
            {
                IEAssessmentId = 2,
                IECategoryId = 3,
                Value = 12.88m,
                Insert_Ts = DateTime.Now
            });
            result.Add(new IEData()
            {
                IEAssessmentId = 2,
                IECategoryId = 4,
                Value = 25.53m,
                Insert_Ts = DateTime.Now
            });
            result.Add(new IEData()
            {
                IEAssessmentId = 2,
                IECategoryId = 5,
                Value = 34.13m,
                Insert_Ts = DateTime.Now
            });
            result.Add(new IEData()
            {
                IEAssessmentId = 2,
                IECategoryId = 6,
                Value = 43.85m,
                Insert_Ts = DateTime.Now
            });
            result.Add(new IEData()
            {
                IEAssessmentId = 2,
                IECategoryId = 7,
                Value = 14.21m,
                Insert_Ts = DateTime.Now
            });
            result.Add(new IEData()
            {
                IEAssessmentId = 2,
                IECategoryId = 8,
                Value = 55.55m,
                Insert_Ts = DateTime.Now
            });
            return result;
        }

        private static List<IEType> GetIETypes()
        {
            List<IEType> result = new List<IEType>()
            {
                new IEType() {Type = "Income" },
                new IEType() {Type = "Expenditure" }
            };

            return result;
        }

        private static List<IECategory> GetIECategories()
        {
            List<IECategory> result = new List<IECategory>();

            result.Add(new IECategory()
            {
                IETypeId = 1,
                Name = "Salary",
                Description = "",
                Order = 1
            });
            result.Add(new IECategory()
            {
                IETypeId = 1,
                Name = "Wages",
                Description = "",
                Order = 2
            });
            result.Add(new IECategory()
            {
                IETypeId = 1,
                Name = "Benefits",
                Description = "",
                Order = 3
            });
            result.Add(new IECategory()
            {
                IETypeId = 2,
                Name = "Housing Expenses",
                Description = "",
                Order = 1
            });
            result.Add(new IECategory()
            {
                IETypeId = 2,
                Name = "Mortgage",
                Description = "",
                Order = 2
            });
            result.Add(new IECategory()
            {
                IETypeId = 2,
                Name = "Rent",
                Description = "",
                Order = 3
            });
            result.Add(new IECategory()
            {
                IETypeId = 2,
                Name = "Essential Bills",
                Description = "",
                Order = 4
            });
            result.Add(new IECategory()
            {
                IETypeId = 2,
                Name = "Council Tax",
                Description = "",
                Order = 5
            });
            result.Add(new IECategory()
            {
                IETypeId = 2,
                Name = "Ultilites and Insurance",
                Description = "",
                Order = 6
            });
            result.Add(new IECategory()
            {
                IETypeId = 1,
                Name = "Credit Commitments",
                Description = "",
                Order = 7
            });
            result.Add(new IECategory()
            {
                IETypeId = 1,
                Name = "Personal Loans",
                Description = "",
                Order = 8
            });
            result.Add(new IECategory()
            {
                IETypeId = 1,
                Name = "Short Term Loans",
                Description = "",
                Order = 9
            });
            result.Add(new IECategory()
            {
                IETypeId = 1,
                Name = "Credit Cards",
                Description = "",
                Order = 10
            });
            result.Add(new IECategory()
            {
                IETypeId = 2,
                Name = "Living Costs",
                Description = "",
                Order = 11
            });
            result.Add(new IECategory()
            {
                IETypeId = 2,
                Name = "Travel",
                Description = "",
                Order = 12
            });
            result.Add(new IECategory()
            {
                IETypeId = 2,
                Name = "Food",
                Description = "",
                Order = 13
            });
            result.Add(new IECategory()
            {
                IETypeId = 2,
                Name = "Childcare",
                Description = "",
                Order = 14
            });
            result.Add(new IECategory()
            {
                IETypeId = 2,
                Name = "Telephone",
                Description = "",
                Order = 15
            });
            result.Add(new IECategory()
            {
                IETypeId = 2,
                Name = "TV",
                Description = "",
                Order = 16
            });
            result.Add(new IECategory()
            {
                IETypeId = 2,
                Name = "Other Expenditure",
                Description = "",
                Order = 17
            });
            result.Add(new IECategory()
            {
                IETypeId = 2,
                Name = "Healthcare",
                Description = "",
                Order = 18
            });
            result.Add(new IECategory()
            {
                IETypeId = 2,
                Name = "Hobbies",
                Description = "",
                Order = 19
            });
            result.Add(new IECategory()
            {
                IETypeId = 2,
                Name = "Gifts",
                Description = "",
                Order = 20
            });

            return result;
        }

        private static List<IEAssessment> GetIEAssessments()
        {
            List<IEAssessment> result = new List<IEAssessment>();
            result.Add(new IEAssessment()
            {
                CustomerId = 1,
                Insert_Ts = DateTime.Now
            });
            result.Add(new IEAssessment()
            {
                CustomerId = 2,
                Insert_Ts = DateTime.Now
            });
            result.Add(new IEAssessment()
            {
                CustomerId = 2,
                Insert_Ts = DateTime.Now
            });
            result.Add(new IEAssessment()
            {
                CustomerId = 3,
                Insert_Ts = DateTime.Now
            });
            result.Add(new IEAssessment()
            {
                CustomerId = 4,
                Insert_Ts = DateTime.Now
            });
            result.Add(new IEAssessment()
            {
                CustomerId = 5,
                Insert_Ts = DateTime.Now
            });

            return result;
        }
    }
}

