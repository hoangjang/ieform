﻿using MiniFormIE.Data.Infrastructure;
using MiniFormIE.Domain;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniFormIE.Data.Repositories
{
    public class AssessmentRepository : RepositoryBase<IEAssessment>, IAssessmentRepository
    {
        public AssessmentRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

    }
    public interface IAssessmentRepository : IRepository<IEAssessment>
    {
    }

}
