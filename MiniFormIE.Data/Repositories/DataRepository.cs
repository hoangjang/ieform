﻿using MiniFormIE.Data.Infrastructure;
using MiniFormIE.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniFormIE.Data.Repositories
{
    public class DataRepository : RepositoryBase<IEData>, IDataRepository
    {
        public DataRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }

    public interface IDataRepository:IRepository<IEData>
    {
    }
}
