﻿using MiniFormIE.Data.Infrastructure;
using MiniFormIE.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniFormIE.Data.Repositories
{
    public class TypeRepository : RepositoryBase<IEType>, ITypeRepository
    {
        public TypeRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }

    public interface ITypeRepository:IRepository<IEType>
    {

    }
}
