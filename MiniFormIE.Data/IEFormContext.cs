﻿using MiniFormIE.Domain;
using MySql.Data.Entity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniFormIE.Data
{
    [DbConfigurationType(typeof(MySqlEFConfiguration))]
    public class IEFormContext:DbContext
    {
        public DbSet<IEType> IETypes { get; set; }
        public DbSet<IECategory> IECategories { get; set; }
        public DbSet<IEAssessment> IEAssessments { get; set; }
        public DbSet<IEData> IEDatas { get; set; }

        public IEFormContext():base("IEFormDB")
        {
            Database.Initialize(false);
            //Database.SetInitializer(new IEDBInitializer());
        }
        public virtual void Commit()
        {
            base.SaveChanges();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
}
